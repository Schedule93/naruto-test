// http://localhost:10/html/con-adm.html

// here is file: server.js 
let router = require("express").Router();
let path = require("path"); 

// Routes :
router.get("/con-adm", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', '/../../html/con-adm.html'));
});

router.get("/adm_panel", function(req, res) {
	res.sendFile(path.join(__dirname, '../public', '/../../html/adm-panel.html'));
});

module.exports = router;

 